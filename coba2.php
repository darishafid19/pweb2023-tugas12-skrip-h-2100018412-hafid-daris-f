<?php

$mahasiswa = array(
    array("Nama" => "Jhanif", "NIM" => "123456", "Jurusan" => "Teknik Informatika"),
    array("Nama" => "hafid", "NIM" => "234567", "Jurusan" => "Sistem Informasi"),
    array("Nama" => "haris", "NIM" => "345678", "Jurusan" => "Teknik Elektro")
);

foreach ($mahasiswa as $data) {
    echo "Nama: " . $data["Nama"] . "<br>";
    echo "NIM: " . $data["NIM"] . "<br>";
    echo "Jurusan: " . $data["Jurusan"] . "<br><br>";
}
?>
